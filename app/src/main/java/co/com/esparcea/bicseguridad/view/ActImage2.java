package co.com.esparcea.bicseguridad.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import android.os.Bundle;

import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;

import co.com.esparcea.bicseguridad.R;


public class ActImage2 extends AppCompatActivity {

    ImageView picture2;
    ImageButton mPickImage2;


    StorageReference mStorage;
    private final int GALLERY_INTENT = 1;
    private ProgressDialog mProgressDialog;
    Uri filepath;
    String downloadUrl;
    DatabaseReference dbref;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_act2);


        picture2 = findViewById(R.id.picture2);
        mPickImage2 = findViewById(R.id.btnPickImage2);
        mProgressDialog = new ProgressDialog(this);



        mStorage = FirebaseStorage.getInstance().getReference();

        dbref = FirebaseDatabase.getInstance().getReference("IMAGEN");



        mPickImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_INTENT);



                mStorage=FirebaseStorage.getInstance().getReference().child("Tarjeta");



            }
        });

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    //borre uri en el ()
    {
        //picture.setImageURI(result);
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == GALLERY_INTENT && resultCode== RESULT_OK){

            mProgressDialog.setTitle("Almacenando...");
            mProgressDialog.setMessage("Subiendo a Firebase");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

            Uri uri = data.getData();
            filepath = data.getData();

            try{
                Bitmap bitmap2 = MediaStore.Images.Media.getBitmap(ActImage2.this.getContentResolver(),filepath);
                picture2.setImageBitmap(bitmap2);
            } catch (FileNotFoundException f ) {
                f.printStackTrace();
            } catch (IOException f) {
                f.printStackTrace();
            }

            StorageReference filePath = mStorage.child("Tarjeta");
            filePath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    mProgressDialog.dismiss();


                    Toast.makeText(ActImage2.this, "La foto se subio correctamente", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }


}


