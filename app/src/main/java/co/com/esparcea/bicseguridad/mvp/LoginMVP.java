package co.com.esparcea.bicseguridad.mvp;

import android.app.Activity;

import java.util.List;

import co.com.esparcea.bicseguridad.model.database.entity.User;


public interface LoginMVP {
    interface Model {

        void validateCredentials(String email, String password, ValidateCredentialsCallBack callback);

        void getAllUsers(GetUserCallBack<List<UserInfo>> callback);

        boolean hasAuthenticatedUser();

        interface ValidateCredentialsCallBack {
            void onSuccess();

            void onFailure();
        }

        interface GetUserCallBack<T> {
            void onSuccess(T data);

            void onFailure();
        }
    }

    interface Presenter {
        void onLoginClick();
        void onFacebookClick();
        void onGoogleClick();
        void onRegisterClick();

        void validateHasUserAuthenticated();
    }

    interface View {

        Activity getActivity();

        LoginInfo getLoginInfo();

        void showEmailError(String error);

        void showPasswordError(String error);

        void showMapUser();

        void showBikeRegisterActivity();

        void showGeneralError(String error);

    }

    class LoginInfo {
        private String email;
        private String password;


        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;

        }

        public String getEmail(){
            return email;
        }

        public String getPassword() {
            return password;
        }
    }


    class UserInfo{
        private String name;
        private String email;


        public UserInfo(String name, String email) {
            this.name = name;
            this.email = email;

        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        @Override
        public String toString() {
            return "UserInfo{" +
                    "name='" + name + '\'' +
                    ", email='" + email + '\'' +
                    '}';
        }
    }


}
