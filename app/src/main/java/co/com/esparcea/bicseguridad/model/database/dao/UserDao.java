package co.com.esparcea.bicseguridad.model.database.dao;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import co.com.esparcea.bicseguridad.model.database.entity.User;

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT * FROM user WHERE email = :email")
    User getUserByEmail(String email);

    @Query("SELECT * FROM user WHERE enable = :enable AND email = :email")
    User getActiveUserByEmailAndEnable(String email, Boolean enable);

    @Insert
    void insert(User user);

    @Update
    void update (User user);

    @Delete
    void delete (User user);
}
