package co.com.esparcea.bicseguridad.presenter;

import co.com.esparcea.bicseguridad.model.BikeRegisterInteractor;
import co.com.esparcea.bicseguridad.mvp.BikeRegisterMVP;

public class BikeRegisterPresenter implements BikeRegisterMVP.Presenter {

    private BikeRegisterMVP.View view;
    private BikeRegisterMVP.Model model;

    public BikeRegisterPresenter(BikeRegisterMVP.View view){
        this.view = view;
        this.model = new BikeRegisterInteractor();
    }

    @Override
    public void onContinueClick() {
        boolean error = false;
        BikeRegisterMVP.BikeRegisterInfo BikeRegisterInfo = view.getBikeRegisterInfo();

        //valida datos
        view.showNameError("");
        view.showEmailError("");
        view.showPasswordError("");

        if (BikeRegisterInfo.getName().isEmpty()){
            view.showNameError("Nombre de usuario es requerido");
            error = true;
        }else if(!isNameValid(BikeRegisterInfo.getName())){
            view.showNameError("Nombre de usuario no es válido");
            error = true;
        }

        if (BikeRegisterInfo.getEmail().isEmpty()){
            view.showEmailError("Correo electrónico es requerido");
            error = true;
        }else if(!isEmailValid(BikeRegisterInfo.getEmail())){
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if (BikeRegisterInfo.getPassword().isEmpty()){
            view.showPasswordError("Contraseña es requerida");
            error = true;
        }else if(!isPasswordValid(BikeRegisterInfo.getPassword())){
            view.showPasswordError("Contraseña no cumple con los criterios de seguridad");
            error = true;
        }

        if (!error) {
            if((model.validateCredentials(BikeRegisterInfo.getName(),BikeRegisterInfo.getEmail(),BikeRegisterInfo.getPassword())));
                view.showActLocation();
        }else{
            view.showGeneralError("Credenciales Inválidas");
        }

    }

    private boolean isNameValid(String name) {
        return name.contains(toString())
        && name.length()>= 3;
    }

    private boolean isPasswordValid(String password) {
        return password.length()>=8 && password.contains("^"+
                "(?=.*[0-9])"+ // Al menos un numero
                "(?=.*[a-z])"+  // al menos una letra
                "(?=.*[A-Z])"+   // al menos una letra mayuscula
                "(?=.*[Q#$%&/()=?¡¿*+{}-.,])"+ // al menos un caracter especial
                "$"
                )
        ;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@")
                && email.endsWith(".com") || email.endsWith("co") ;

    }
}
