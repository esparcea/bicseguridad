package co.com.esparcea.bicseguridad.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import co.com.esparcea.bicseguridad.BtnPanicoActivity;
import co.com.esparcea.bicseguridad.R;
import co.com.esparcea.bicseguridad.model.repository.FirebaseAuthRepository;


public class MapUser extends AppCompatActivity {

    AppCompatButton btnContinue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_user);

        btnContinue = findViewById(R.id.btn_map);
        btnContinue.setOnClickListener(v -> onContinueClick());

    }

    public void onBackPressed(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.app_name)
                .setMessage("Desea cerrar la sesión")
                .setPositiveButton("SI",
                        (dialog, which) -> {
                            FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance();
                            repository.logOut();

                            MapUser.super.onBackPressed();
                        })
                .setNegativeButton("NO",null);

        builder.create().show();
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void onContinueClick() {
        Intent intent= new Intent( MapUser.this, BtnPanicoActivity.class);
        startActivity(intent);
    }



    }
