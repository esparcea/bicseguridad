package co.com.esparcea.bicseguridad.model.repository;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseAuthRepository {
    private static FirebaseAuthRepository instance;

    public static FirebaseAuthRepository getInstance() {
        if (instance == null){
            instance = new FirebaseAuthRepository();
        }
        return instance;
    }

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    private FirebaseAuthRepository(){
        mAuth = FirebaseAuth.getInstance();
    }

    public boolean isAuthenticated(){
        return getCurrentUser() != null;
    }

    public FirebaseUser getCurrentUser() {
        if (currentUser == null){
            currentUser = mAuth.getCurrentUser();
        }
        return currentUser;
    }

    public void createUser(String email, String password, FirebaseAuthCallback callback){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }

    public void validateUser(String email, String password, FirebaseAuthCallback callback){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }

    public void logOut() {
        if (currentUser != null){
            mAuth.signOut();
            currentUser = null;
        }
    }

    public interface FirebaseAuthCallback{
        void onSuccess();

        void onFailure();
    }
}
