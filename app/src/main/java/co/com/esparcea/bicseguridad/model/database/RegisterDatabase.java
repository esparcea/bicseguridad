package co.com.esparcea.bicseguridad.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import co.com.esparcea.bicseguridad.model.database.dao.UserDao;
import co.com.esparcea.bicseguridad.model.database.entity.Role;
import co.com.esparcea.bicseguridad.model.database.entity.User;

@Database(entities = {User.class}, version = 1)
public abstract class RegisterDatabase extends RoomDatabase {

        private  volatile static RegisterDatabase instance;


        public static RegisterDatabase getInstance(Context context){
            if (instance == null) {
                instance = Room.databaseBuilder(context.getApplicationContext(),
                        RegisterDatabase.class,
                        "Users-database")
                        .allowMainThreadQueries()
                        .build();


            }
            return instance;

        }



    public abstract UserDao getUserDao();

}
