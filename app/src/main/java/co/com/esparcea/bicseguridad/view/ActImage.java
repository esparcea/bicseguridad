package co.com.esparcea.bicseguridad.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import android.os.Bundle;

import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;

import co.com.esparcea.bicseguridad.R;


public class ActImage extends AppCompatActivity {

    ImageView mpicture;
    ImageButton mPickImage;
    StorageReference mStorage;
    private final int GALLERY_INTENT = 1;
    private ProgressDialog mProgressDialog;
    Uri filepath;
    String downloadUrl;


    DatabaseReference dbref;





    ActivityResultLauncher <String> mGetContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_act);

        mStorage = FirebaseStorage.getInstance().getReference();
        dbref = FirebaseDatabase.getInstance().getReference("IMAGE");


        mpicture = findViewById(R.id.picture);
        mProgressDialog = new ProgressDialog(this);
        mPickImage = findViewById(R.id.btnPickImage);




        //mGetContent= registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {

        //});

        mPickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent (Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_INTENT);
                //mGetContent.launch("image/*");


                mStorage=FirebaseStorage.getInstance().getReference().child("Bicicleta");
                mStorage.child("Foto").getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        downloadUrl=task.getResult().toString();
                        dbref.push().child("imageurl").setValue(downloadUrl);
                        mProgressDialog.dismiss();
                        Toast.makeText(ActImage.this, "Imagen subida", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

    }










    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    //borre uri en el ()
    {
        //picture.setImageURI(result);
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == GALLERY_INTENT && resultCode== RESULT_OK){

            mProgressDialog.setTitle("Almacenando...");
            mProgressDialog.setMessage("Subiendo a Firebase");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

            Uri uri = data.getData();
            filepath = data.getData();
            try{
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(ActImage.this.getContentResolver(),filepath);
                mpicture.setImageBitmap(bitmap);
            } catch (FileNotFoundException e ) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            StorageReference filePath = mStorage.child("Foto de la bicicleta").child(uri.getLastPathSegment());
            filePath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    mProgressDialog.dismiss();


                    Toast.makeText(ActImage.this, "La foto se subio correctamente", Toast.LENGTH_SHORT).show();
                }
            });
        }


    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }


}
