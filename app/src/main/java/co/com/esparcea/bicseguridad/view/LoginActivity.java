package co.com.esparcea.bicseguridad.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import co.com.esparcea.bicseguridad.R;
import co.com.esparcea.bicseguridad.mvp.LoginMVP;
import co.com.esparcea.bicseguridad.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;


    AppCompatButton btnLogin;
    AppCompatButton btnFacebook;
    AppCompatButton btnGoogle;
    AppCompatButton btnRegister;

    private LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter( this);
        presenter.validateHasUserAuthenticated();

        initUI();

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void initUI() {

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);


        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> presenter.onLoginClick());

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(v -> presenter.onFacebookClick());

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(v -> presenter.onGoogleClick());


        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(v -> presenter.onRegisterClick());
    }


    @Override
    public Activity getActivity() {
        return LoginActivity.this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(
                etEmail.getText().toString().trim(),
        etPassword.getText().toString().trim());
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);

    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);

    }

    @Override
    public void showMapUser() {
        Intent intent = new Intent (LoginActivity.this, MapUser.class);
        startActivity(intent);
    }

    @Override
    public void showBikeRegisterActivity() {
        Intent intent = new Intent (LoginActivity.this, BikeRegisterActivity.class);
        startActivity(intent);

    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }


}