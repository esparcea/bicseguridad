package co.com.esparcea.bicseguridad;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class BtnPanicoActivity extends AppCompatActivity {


    AppCompatButton btn_denuncia_en_linea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btn_panico);

        btn_denuncia_en_linea = findViewById(R.id.btn_denuncia_en_linea);
        btn_denuncia_en_linea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BtnPanicoActivity.this, DenunciaActivity.class);
                startActivity(intent);
            }
        });
    }
}