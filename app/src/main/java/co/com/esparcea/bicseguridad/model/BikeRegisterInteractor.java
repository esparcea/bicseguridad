package co.com.esparcea.bicseguridad.model;

import java.util.HashMap;
import java.util.Map;

import co.com.esparcea.bicseguridad.mvp.BikeRegisterMVP;

public class BikeRegisterInteractor implements BikeRegisterMVP.Model {

    private Map<String, String> users;

    public BikeRegisterInteractor() {
        users = new HashMap<>();
        users.put("test1@email.com", "");
        users.put("test2@email.com", "12345678");
    }

    @Override
    public boolean validateCredentials(String name, String email, String password) {
        return  (users.get(email) != null
                && users.get(email).equals(password));
    }
}
