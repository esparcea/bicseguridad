package co.com.esparcea.bicseguridad.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;


import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import co.com.esparcea.bicseguridad.R;
import co.com.esparcea.bicseguridad.mvp.BikeRegisterMVP;
import co.com.esparcea.bicseguridad.presenter.BikeRegisterPresenter;


public class BikeRegisterActivity extends AppCompatActivity implements BikeRegisterMVP.View {

    private TextInputLayout tilName;
    private TextInputEditText etName;
    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;

    private BikeRegisterMVP.Presenter presenter;

    AppCompatButton btnContinue;
    AppCompatImageButton button1;
    AppCompatImageButton button2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bikeregister);



        presenter = new BikeRegisterPresenter( BikeRegisterActivity.this);

        initUI();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void initUI() {
        tilName = findViewById(R.id.til_name);
        etName = findViewById(R.id.et_name);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnContinue = findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(v -> presenter.onContinueClick());

        ImageButton button1 = (ImageButton) findViewById(R.id.upload_bikePhoto);
        button1.setOnClickListener(v -> showActImage());


        ImageButton button2 = (ImageButton) findViewById(R.id.upload_cardPhoto);
        button2.setOnClickListener(v -> showActImage2());



    }

    private void showActImage2() {
        Intent intent= new Intent( BikeRegisterActivity.this, ActImage2.class);
        startActivity(intent);
    }

    private void showActImage() {
        Intent intent = new Intent(BikeRegisterActivity.this, ActImage.class);
        startActivity(intent);
    }

    @Override
    public BikeRegisterMVP.BikeRegisterInfo getBikeRegisterInfo() {
        return new BikeRegisterMVP.BikeRegisterInfo(
                etName.getText().toString().trim(),
                etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim());
    }

    @Override
    public void showNameError(String error) {
        tilName.setError(error);
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showActLocation() {
        Intent intent = new Intent(BikeRegisterActivity.this, ActLocation.class);
        startActivity(intent);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(BikeRegisterActivity.this, error, Toast.LENGTH_SHORT).show();
    }
}