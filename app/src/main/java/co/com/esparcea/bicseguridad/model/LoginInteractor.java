package co.com.esparcea.bicseguridad.model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.com.esparcea.bicseguridad.model.database.entity.User;
import co.com.esparcea.bicseguridad.model.repository.FirebaseAuthRepository;
import co.com.esparcea.bicseguridad.model.repository.UserRepository;
import co.com.esparcea.bicseguridad.mvp.LoginMVP;

public class LoginInteractor implements LoginMVP.Model {

    private final UserRepository userRepository;
    private final FirebaseAuthRepository firebaseAuthRepository;

    //private Map<String, String> users;

    public LoginInteractor(Context context){
        userRepository = new UserRepository(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance();



        //users = new HashMap<>();
        //users.put("test1@email.com","");
        //users.put("test2@email.com","12345678");
    }


    @Override
    public void validateCredentials (String email, String password, ValidateCredentialsCallBack callback) {

        /*
        // Probar la creación de un usuario
        firebaseAuthRepository.createUser(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        Log.i(LoginInteractor.class.getSimpleName(), "Usuario creado exitosamente");
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        Log.e(LoginInteractor.class.getSimpleName(), "Usuario no creado");
                        callback.onFailure();
                    }
                });

         */


        // Probar la validación de un usuario
        firebaseAuthRepository.validateUser(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        callback.onFailure();
                    }
                });

        /*

        userRepository.getUserByEmail(email, new UserRepository.UserCallback<User>(){
            @Override
            public void onSuccess(User user) {
                if (user != null && user.getPassword().equals(password)){
                    callback.onSuccess();
                } else {
                    callback.onFailure();
                }

            }

            @Override
            public void onFailure() {
                callback.onFailure();

            }
        });


         */
    }

    @Override
    public void getAllUsers(GetUserCallBack <List<LoginMVP.UserInfo>> callback) {
        userRepository.getAllUsers(new UserRepository.UserCallback<List<User>>() {
            @Override
            public void onSuccess(List<User> values) {
                List<LoginMVP.UserInfo> users = new ArrayList<>();
                for (User user: values){
                    if(user.getEnable() == null || !user.getEnable()){
                        continue;
                    }
                    users.add(new LoginMVP.UserInfo(user.getName(), user.getEmail()));

                }
                callback.onSuccess(users);

            }

            @Override
            public void onFailure() {
                callback.onFailure();

            }
        });
    }

    @Override
    public boolean hasAuthenticatedUser() {
        return firebaseAuthRepository.isAuthenticated();
    }


}
