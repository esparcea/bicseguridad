package co.com.esparcea.bicseguridad.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import co.com.esparcea.bicseguridad.model.database.RegisterDatabase;
import co.com.esparcea.bicseguridad.model.database.dao.UserDao;
import co.com.esparcea.bicseguridad.model.database.entity.User;

public class UserRepository {

    private final UserDao userDao;
    private final DatabaseReference userRef;

    private final Boolean inDB = false;

    public UserRepository(Context context) {
        this.userDao = RegisterDatabase.getInstance(context)
        .getUserDao();


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("users");

        //loadInitialUsers();


    }


    private void loadInitialUsers(){

        if (inDB) {
            userDao.insert(new User("Pepito Perez", "pepito@email.com", "12345678"));
            userDao.insert(new User("User Test", "test@email.com", "87654321"));
        } else {
            //usar firebase
            userRef.child("test_email_com").child("name").setValue("User Test");
            userRef.child("test_email_com").child("email").setValue("test@email.com");
            userRef.child("test_email_com").child("password").setValue("87654321");

            User user = new User("Pepito Perez", "pepito@email.com", "12345678");
            userRef.child(getEmailId(user.getEmail())).setValue(user);

        }
    }



    public void getAllUsers(UserCallback<List<User>> callback){
        if(inDB){
            callback.onSuccess(userDao.getAll());
        } else {

            userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    userRef.removeEventListener(this);
                    List<User> users = new ArrayList<>();
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        users.add(snapshot.getValue(User.class));
                    }
                    callback.onSuccess(users);

                }


                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    userRef.removeEventListener( this);
                    callback.onFailure();
                }
            });
        }
    }








    public void getUserByEmail(String email, UserCallback<User> callback) {

        if (inDB) {
            callback.onSuccess(userDao.getUserByEmail(email));

        }else {

            userRef.child(getEmailId(email)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    User user = dataSnapshot.getValue(User.class);
                    callback.onSuccess(user);
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    callback.onFailure();

                }
            });
        }



    }


    private String getEmailId(String email){
        return email.replace( '@', '_').replace('.','_');
    }


    public static interface UserCallback <T>{
        void onSuccess(T node);
        void onFailure();

    }
}
