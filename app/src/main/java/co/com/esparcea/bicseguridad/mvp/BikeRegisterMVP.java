package co.com.esparcea.bicseguridad.mvp;

public interface BikeRegisterMVP {

    interface Model{

        boolean validateCredentials(String name, String email, String password);
    }

    interface Presenter{
        void onContinueClick();

    }

    interface View{
        BikeRegisterInfo getBikeRegisterInfo();

        void showNameError(String error);

        void showEmailError(String error);

        void showPasswordError(String error);

        void showActLocation();

        void showGeneralError(String error);
    }

    class BikeRegisterInfo{
        private String name;
        private String email;
        private String password;

        public BikeRegisterInfo(String name, String email, String password) {
            this.name = name;
            this.email = email;
            this.password = password;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
