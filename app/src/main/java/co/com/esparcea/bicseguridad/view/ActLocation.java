package co.com.esparcea.bicseguridad.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;


import android.content.Intent;
import android.os.Bundle;

import co.com.esparcea.bicseguridad.R;


public class ActLocation extends AppCompatActivity {

    AppCompatButton btnActLoc;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_location);


        btnActLoc = findViewById(R.id.btn_actLoc);
        btnActLoc.setOnClickListener(v -> onActLocClick());


    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void onActLocClick() {
        Intent intent= new Intent( ActLocation.this, MapUser.class);
        startActivity(intent);

    }
}