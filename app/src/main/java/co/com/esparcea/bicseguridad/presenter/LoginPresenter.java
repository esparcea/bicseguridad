package co.com.esparcea.bicseguridad.presenter;

import android.util.Log;

import java.util.List;

import co.com.esparcea.bicseguridad.model.LoginInteractor;
import co.com.esparcea.bicseguridad.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {
    private LoginMVP.View view;
    private LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view){
        this.view = view;
        this.model = new LoginInteractor(view.getActivity());
    }

    @Override
    public void onLoginClick() {
        boolean error = false;
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();
        //validacion de datos
        view.showEmailError("");
        view.showPasswordError("");
        if (loginInfo.getEmail().isEmpty()){
            view.showEmailError("Correo electrónico es obligatorio");
            error = true;
        } else if (!isEmailValid(loginInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if (loginInfo.getPassword().isEmpty()){
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        } else if (!isPasswordValid(loginInfo.getPassword())){
            view.showPasswordError("Contraseña insegura");
            error = true;
        }


        if(!error) {

            new Thread(() ->{


                model.validateCredentials(loginInfo.getEmail(), loginInfo.getPassword(), new LoginMVP.Model.ValidateCredentialsCallBack() {
                    @Override
                    public void onSuccess() {
                        view.getActivity().runOnUiThread(()-> {
                            view.showMapUser();
                        });


                    }

                    @Override
                    public void onFailure() {
                        view.getActivity().runOnUiThread(()->{
                            view.showGeneralError("Credenciales invalidas");
                        });


                    }
                });

            }).start();



            new Thread(() ->{
                model.getAllUsers(new LoginMVP.Model.GetUserCallBack<List<LoginMVP.UserInfo>>() {
                    @Override
                    public void onSuccess(List<LoginMVP.UserInfo> data) {
                        for (LoginMVP.UserInfo user: data){
                            Log.i(LoginPresenter.class.getSimpleName(), user.toString());
                        }

                    }

                    @Override
                    public void onFailure() {
                        Log.e(LoginPresenter.class.getSimpleName(), "No hay datos en base de datos");
                    }

                });

            }).start();

        }
    }

    private boolean isPasswordValid(String password) {
            return password.length() >= 8;
    }



        private boolean isEmailValid(String email) {
        return email.contains("@")
                && email.endsWith(".com");
    }

    @Override
    public void onFacebookClick() {

    }

    @Override
    public void onGoogleClick() {

    }

    @Override
    public void onRegisterClick() {
        view.showBikeRegisterActivity();

    }

    @Override
    public void validateHasUserAuthenticated() {
        if(model.hasAuthenticatedUser()){
            view.showMapUser();
        }
    }


}

